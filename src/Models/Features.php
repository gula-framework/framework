<?php


namespace Gula\Framework\Models;


use Illuminate\Database\Eloquent\Factories\HasFactory;

class Features extends \Illuminate\Database\Eloquent\Model
{
    use HasFactory;

    protected $guarded = [];

}
