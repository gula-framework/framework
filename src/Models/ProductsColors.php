<?php


namespace Gula\Framework\Models;


use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class ProductsColors extends \Illuminate\Database\Eloquent\Model
{
    use HasFactory;

    protected $guarded = [];

    /**
     * @param Request $request
     * @param int $idProduct
     * @param array $colors
     */
    public function updateProductLinks(Request $request, int $idProduct, array $colors)
    {
        $this->where('id_product', '=', $idProduct)->delete();

        foreach ($colors as $idColor => $color){
            $link = new $this;
            $link->id_product = $idProduct;
            $link->id_color = $idColor;
            $link->save();
        }
    }

}
