<?php

namespace Gula\Framework\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

class Cart extends \Illuminate\Database\Eloquent\Model
{
    use HasFactory;

    protected $guarded = [];
    protected $table = 'cart';

}
