<?php


namespace Gula\Framework\Models;


use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * @property int $id
 * @property int $id_category
 */
class Products extends \Illuminate\Database\Eloquent\Model
{
    use HasFactory;

    protected $guarded = [];


    public function getPrice()
    {
        $price = (new Prices())->where('id_product', '=', $this->id)->first();

        return $price->price;
    }
}
