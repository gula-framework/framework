<?php


namespace Gula\Framework\Models;


use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Prices extends \Illuminate\Database\Eloquent\Model
{
    use HasFactory;

    protected $guarded = [];

    /**
     * @param Collection $priceSet
     * @return array
     */
    public function reorderPriceList(Collection $priceSet): array
    {
        $prices = [];

        //default first
        foreach ($priceSet as $price)
        {
            if($price->name === 'default'){
                $prices[] = [
                    'name' => 'default',
                    'price' => $price->price
                ];
                continue;
            }
        }

        if(count($prices) === 0 ){
            $prices[] = [
                'name' => 'default',
                'price' => 0
            ];
        }

        foreach ($priceSet as $price){
            if($price['name'] !== 'default'){

                $prices[] = [
                    'name' => $price->name,
                    'price' => $price->price
                ];
            }
        }

        while (count($prices) < 10) {
            $prices[] = ['name' => null, 'price' => null];
        }

        return $prices;
    }

    public function updatePrices(int $idProduct, array $prices)
    {
        $this->where('id_product', '=', $idProduct)->delete();

        foreach ($prices as $price){
            if(!empty($price['name']) && $price['price'] > 0){

                $link = new $this;
                $link->id_product = $idProduct;
                $link->name = $price['name'];
                $link->price = $price['price'];
                $link->save();
            }
        }
    }
}
