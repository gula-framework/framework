<?php


namespace Gula\Framework\Controllers;


use Illuminate\Http\Request;

class ShippingCosts extends \App\Http\Controllers\Controller
{
    protected $modelName = \Gula\Framework\Models\ShippingCosts::class;

    public function index(Request $request, int $pageNumber = 1)
    {
        $params = [
            'pageNumber' => $pageNumber,
            'modelName' => $this->modelName,
            'page' => $pageNumber,
            'listOrder' => ['name' => 'asc'],
        ];

        return (new ListFilter())->getList($params);
    }

    public function add(Request $request)
    {
        $newRecord = new \Gula\Framework\Models\ShippingCosts();
        $newRecord->price = 0;
        $newRecord->save();

        return redirect('cms/shippingcosts/edit/' . $newRecord->id);
    }

    public function edit(Request $request, int $id)
    {
        $record = (new \Gula\Framework\Models\ShippingCosts())->where('id', '=', $id)->first();

        $meta = [
            'title' => 'Bezorgkosten',
            'icon' => 'https://cms.gula.nl/resizer/36x36/cms/icons/delivery.png',
        ];

        return view('framework::edit_shipping_costs', compact('record', 'meta'));
    }

    public function store(Request $request)
    {
        $post = $request->all();
        unset($post['_token']);

        $record = (new \Gula\Framework\Models\ShippingCosts())->where('id', '=', $post['id']);
        $record->update($post);

        return redirect('/cms/shippingcosts/list');

    }

}
