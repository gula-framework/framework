<?php


namespace Gula\Framework\Controllers;


use Illuminate\Http\Request;

class Categories extends \App\Http\Controllers\Controller
{
    protected $modelName = \Gula\Framework\Models\Categories::class;

    public function index(Request $request, int $pageNumber = 1)
    {
        $params = [
            'pageNumber' => $pageNumber,
            'modelName' => $this->modelName,
            'page' => $pageNumber,
            'listOrder' => ['name' => 'asc'],
        ];

        return (new ListFilter())->getList($params);
    }

    public function add(Request $request)
    {
        $newRecord = new \Gula\Framework\Models\Categories();
        $newRecord->save();

        return redirect('cms/categories/edit/' . $newRecord->id);
    }

    public function edit(Request $request, int $id)
    {
        $record = (new \Gula\Framework\Models\Categories())->where('id', '=', $id)->first();
        $categories = (new \Gula\Framework\Models\Categories())->where('deleted', '=', false)->get();

        if(!$record->slug && $record->name !== null){
            $record->slug = slugify($record->name);
        }

        $meta = [
            'title' => 'Categorieën',
            'icon' => 'https://cms.gula.nl/resizer/36x36/cms/icons/categories.png',
            'images' => [],
//            'images' => $mdlImages->getImageList(env('PATH_IMAGE_UPLOAD') . '/categorie'),

        ];

        return view('framework::edit_categories', compact('record', 'meta', 'categories'));
    }

    public function store(Request $request)
    {
        $post = $request->all();
        unset($post['_token']);

        if(!$post['slug']){
            $post['slug'] = slugify($post['name']);
        }

        $record = (new \Gula\Framework\Models\Categories())->where('id', '=', $post['id']);
        $record->update($post);

        return redirect('/cms/categories/list');

    }
}
