<?php

namespace Gula\Framework\Controllers;

use App\Models\User;
use Gula\Framework\Models\Images;
use Illuminate\Http\Request;
//use Gula\Framework\Models\Cart;
class Cart extends \App\Http\Controllers\Controller
{
    /**
     * @param Request $request
     * @return bool
     */
    public function addToCart(Request $request)
    {
        $post = $request->post();

        $cart = new \Gula\Framework\Models\Cart();
        $cart->fill($post);
        $cart->save();

        return true;
    }

    /**
     * @param Request $request
     * @return bool
     */
    public function deleteFromCart(Request $request)
    {
        $post = $request->post();

        (new \Gula\Framework\Models\Cart())
            ->where('id', '=', $post['id_cart'])
            ->where('id_product', '=', $post['id_product'])
            ->where('id_session', '=', $post['id_session'])
            ->update(['deleted' => true]);

        return true;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function getCartMenu(Request $request)
    {
        $idSession = $request->id_session;
//        $idSession = 'tN9NQ9vQNw4bhpQLeJSNkdmxpbjEjCh2fmhzEzfI';
        //TODO verwijderen

        $cart = (new \Gula\Framework\Models\Cart())
            ->select('cart.*', 'products.name', 'products.slug', 'prices.price')
            ->join('products', 'products.id', '=', 'cart.id_product')
            ->join('prices', 'prices.id_product', 'cart.id_product')
            ->where('id_session', '=', $idSession)
            ->where('cart.deleted', '=', false)
            ->where('prices.name', '=', 'default')
            ->get();

        $cartTotals = $this->getCartTotal($cart);
        $cart = $this->addMainImages($cart);

        return view('partials.cart_menu', compact('cart', 'cartTotals'));
    }

    /**
     * @param $cart
     * @return array
     */
    private function getCartTotal($cart): array
    {
        $number = count($cart);
        $amount = 0;

        foreach ($cart as $item){
            $amount+= ($item->amount * $item->price);
        }

        return ['number' => $number, 'amount' => $amount];
    }

    /**
     * @param $cart
     * @return mixed
     */
    private function addMainImages($cart)
    {
        foreach ($cart as &$item){
            $image = (new Images())
                ->where('id_item', '=', $item->id_product)
                ->where('table','=', 'products')
                ->where('deleted', '=', false)
                ->orderBy('sort_order', 'asc')
                ->first();

            $item->image_main = $image->url;
        }

        return $cart;
    }
}
