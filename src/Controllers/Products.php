<?php


namespace Gula\Framework\Controllers;


use Gula\Framework\Models\Prices;
use Gula\Framework\Models\ProductsColors;
use Gula\Framework\Models\ProductsMaterials;
use Gula\Framework\Models\ProductsPrices;
use Illuminate\Http\Request;

class Products extends \App\Http\Controllers\Controller
{
    protected $modelName = \Gula\Framework\Models\Products::class;

    public function index(Request $request, int $pageNumber = 1)
    {
        $params = [
            'pageNumber' => $pageNumber,
            'modelName' => $this->modelName,
            'page' => $pageNumber,
            'columns' => [
                [
                    'name' => 'id',
                    'filter' => false
                ],
                [
                    'name' => 'name',
                    'filter' => true
                ],
                [
                    'name' => 'id_category',
                    'table' => 'categories',
                    'filter' => true
                ],
            ],
            'join' => [
                'table' => 'categories',
                'on_left' => 'id_category',
                'on_right' => 'id',
                'columns' => ['name'],
            ],
            'listOrder' => ['name' => 'asc'],
        ];

        return (new ListFilter())->getList($params);
    }

    public function add(Request $request)
    {
        $newRecord = new \Gula\Framework\Models\Products();
        $newRecord->save();

        return redirect('cms/products/edit/' . $newRecord->id);
    }

    public function edit(Request $request, int $id)
    {
        $record = (new \Gula\Framework\Models\Products())->where('id', '=', $id)->first();
        $categories = (new \Gula\Framework\Models\Categories())->where('deleted', '=', false)->orderBy('name', 'asc')->get();
        $colors = (new \Gula\Framework\Models\Colors())->where('deleted', '=', false)->orderBy('name', 'asc')->get();
        $productColors = (new ProductsColors())->where('id_product', '=', $id)->get('id_color');
        $images = (new \Gula\Framework\Models\Images())
            ->where('id_item', '=', $id)
            ->where('deleted', '=', false)
            ->where('table', '=', 'products')
            ->orderBy('sort_order', 'asc')
            ->orderBy('id', 'asc')
            ->get();

        foreach ($colors as &$color) {
            foreach ($productColors as $productColor) {
                if ($productColor->id_color === $color->id) {
                    $color->selected = true;
                }
            }
        }

        $materials = (new \Gula\Framework\Models\Materials())->where('deleted', '=', false)->orderBy('name', 'asc')->get();
        $productsMaterials = (new ProductsMaterials())->where('id_product', '=', $id)->get('id_material');

        foreach ($materials as &$material) {
            foreach ($productsMaterials as $productsMaterial) {
                if ($productsMaterial->id_material === $material->id) {
                    $material->selected = true;
                }
            }
        }

        $mdlPrices = new Prices();
        $priceSet = $mdlPrices->where('deleted', '=', false)->where('id_product', '=', $id)->orderBy('price', 'asc')->get();
        $prices = $mdlPrices->reorderPriceList($priceSet);

        $deliveryTimes = (new \Gula\Framework\Models\DeliveryTimes())->where('deleted', '=', false)->orderBy('name', 'asc')->get();
        $shippingCosts = (new \Gula\Framework\Models\ShippingCosts())->where('deleted', '=', false)->orderBy('name', 'asc')->get();

        $meta = [
            'title' => 'Product',
            'icon' => 'https://cms.gula.nl/resizer/36x36/cms/icons/product.png',
        ];

        return view('framework::edit_products', compact('record', 'meta', 'categories', 'colors', 'materials', 'prices', 'shippingCosts', 'deliveryTimes', 'images'));
    }

    public function store(Request $request)
    {
        $post = $request->all();

        if (!$post['slug']) {
            $post['slug'] = slugify($post['name']);
        }

        (new ProductsColors())->updateProductLinks($request, $post['id'], $post['colors'] ?? []);
        (new ProductsMaterials())->updateProductLinks($request, $post['id'], $post['materials'] ?? []);
        (new Prices())->updatePrices($post['id'], $post['prices'] ?? []);

        $newImages = [];

        foreach ($post as $key => $item) {
            if (substr($key, 0, 6) === 'image_' && $item !== null) {
                $newImages[] = $item;
                unset($post[$key]);
            }
        }

        (new Images())->updateLinkedImages($newImages, $post['id'], 'products');

        unset($post['_token']);
        unset($post['colors']);
        unset($post['materials']);
        unset($post['prices']);

        $record = (new \Gula\Framework\Models\Products())->where('id', '=', $post['id']);
        $record->update($post);

        return redirect('/cms/products/list');
    }

    public function unlinkImage($idImage, $idEntity)
    {
        (new Images())->unlinkImage($idImage, $idEntity, 'products');

        return redirect('/cms/products/edit/' . $idEntity);
    }

    public function getProduct(Request $request, int $idProduct)
    {
        return json_encode(['test']);
    }
}
