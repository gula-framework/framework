<?php


namespace Gula\Framework\Controllers;


use Gula\Fileupload\Controllers\FileuploadController;
use Illuminate\Http\Request;

class Images extends \App\Http\Controllers\Controller
{
//    public function index(Request $request, string $folder = null, string $subfolder = null)
//    {
//        $folder .= ($subfolder !== null ? '/' . $subfolder : '');
//        $fileManager = new FileuploadController();
//
//        return $fileManager->getFolder($folder);
//    }

    public function getImagesMeta(Request $request)
    {
//        echo $request->post('folder');exit();
        $folder = $request->post('folder');

        $fileManager = new FileuploadController();

        return $fileManager->getFolderMeta($folder);
    }

    public function updateLinkedImages(array $images, int $idEntity, $table)
    {
        foreach ($images as $image) {
            $image = str_replace($table . '/', '', $image);

            $delete = (new \Gula\Framework\Models\Images())
                ->where('id_item', '=', $idEntity)
                ->where('table', '=', $table)
                ->where('url', '=', $image)
                ->delete();

            $newImage = new \Gula\Framework\Models\Images();
            $newImage->url = $image;
            $newImage->id_item = $idEntity;
            $newImage->table = $table;
            $newImage->sort_order = 1;
            $newImage->save();
        }
    }

    public function unlinkImage($idImage, $idEntity, $table)
    {
        (new \Gula\Framework\Models\Images())
            ->where('id', '=', $idImage)
            ->where('id_item', '=', $idEntity)
            ->where('table', '=', $table)
            ->update(['deleted' => true]);
    }

    public function imageManager(string $folder = null, string $subfolder = null)
    {
        $folder .= ($subfolder !== null ? '/' . $subfolder : '');
        $object = new FileuploadController();

        return $object->getFolder($folder);
    }

}
