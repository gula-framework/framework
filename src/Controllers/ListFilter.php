<?php


namespace Gula\Framework\Controllers;


class ListFilter extends \App\Http\Controllers\Controller
{
    protected $itemsPerPage = 30;

    public function getList(array $params)
    {
        $data = [];
        $model = new $params['modelName'];
        $pageNumber = ($params['pageNumber'] ?? 1);
        $listOrders = ($params['listOrder'] ?? ['column' => 'name', 'order' => 'ASC']);

        if (isset($params['itemsPerPage'])) {
            $this->itemsPerPage = $params['itemsPerPage'];
        }

        $model->where('deleted', '=', false);
        $model->where('active', '=', true);

        $arrTitle = explode('\\', $params['modelName']);

        $data = [
            'listCount' => $model->count(),
            'pageNumber' => $pageNumber,
            'pages' => ceil($model->count() / $this->itemsPerPage),
            'icon' => (isset($params['icon']) ? $params['icon'] : null),
            'columns' => ($params['columns'] ?? [
                    [
                        'name' => 'id',
                        'filter' => false
                    ],
                    [
                        'name' => 'name',
                        'filter' => true
                    ],
                ]),
            'title' => strtoupper(end($arrTitle)),
            'model' => strtolower(end($arrTitle)),
        ];

        foreach ($listOrders as $column => $order) {
            $model->orderBy($column, $order);
        }

        $model->skip(($pageNumber - 1) * $this->itemsPerPage);
        $model->take($this->itemsPerPage);
        $data['list'] = $model->get();

        return view('framework::list', compact('data'));
//        return $data;
    }
}
