<?php

namespace Gula\Framework\Controllers;

use Illuminate\Http\Request;

class Materials extends \App\Http\Controllers\Controller
{
    protected $modelName = \Gula\Framework\Models\Materials::class;

    public function index(Request $request, int $pageNumber = 1)
    {
        $params = [
            'pageNumber' => $pageNumber,
            'modelName' => $this->modelName,
            'page' => $pageNumber,
            'listOrder' => ['name' => 'asc'],
        ];

        return (new ListFilter())->getList($params);
    }

    public function add(Request $request)
    {
        $newRecord = new \Gula\Framework\Models\Materials();
        $newRecord->save();

        return redirect('cms/materials/edit/' . $newRecord->id);
    }

    public function edit(Request $request, int $id)
    {
        $record = (new \Gula\Framework\Models\Materials())->where('id', '=', $id)->first();
        $materials = (new \Gula\Framework\Models\Materials())->where('deleted', '=', false)->get();

        $meta = [
            'title' => 'Materiaal',
            'icon' => 'https://cms.gula.nl/resizer/36x36/cms/icons/materials.png',
        ];

        return view('framework::edit_materials', compact('record', 'meta'));
    }

    public function store(Request $request)
    {
        $post = $request->all();
        unset($post['_token']);

        $record = (new \Gula\Framework\Models\Materials())->where('id', '=', $post['id']);
        $record->update($post);

        return redirect('/cms/materials/list');

    }

}
