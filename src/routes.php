<?php

Route::group(['middleware' => 'web', 'namespace' => 'Gula\Framework\Controllers'], function()
{
    Route::get('cms', ['uses' => 'CmsController@index']);

    //cart
    Route::post('/api/add-to-cart', 'Cart@addToCart');
    Route::post('/api/delete-from-cart', 'Cart@deleteFromCart');
    Route::get('/api/get-cart-menu', 'Cart@getCartMenu');

//    categories
    Route::get('cms/categories/edit/{id}', ['uses' => 'Categories@edit']);
    Route::get('cms/categories/add', ['uses' => 'Categories@add']);
    Route::post('cms/categories/store', ['uses' => 'Categories@store']);
    Route::get('cms/categories/list', ['uses' => 'Categories@index']);
    Route::get('cms/categories/list/{pageNumber}', ['uses' => 'Categories@index']);

//    products
    Route::get('cms/products/edit/{id}', ['uses' => 'Products@edit']);
    Route::get('cms/products/add', ['uses' => 'Products@add']);
    Route::post('cms/products/store', ['uses' => 'Products@store']);
    Route::get('cms/products/list', ['uses' => 'Products@index']);
    Route::get('cms/products/list/{pageNumber}', ['uses' => 'Products@index']);

//    colors
    Route::get('cms/colors/edit/{id}', ['uses' => 'Colors@edit']);
    Route::get('cms/colors/add', ['uses' => 'Colors@add']);
    Route::post('cms/colors/store', ['uses' => 'Colors@store']);
    Route::get('cms/colors/list', ['uses' => 'Colors@index']);
    Route::get('cms/colors/list/{pageNumber}', ['uses' => 'Colors@index']);

//    materials
    Route::get('cms/materials/edit/{id}', ['uses' => 'Materials@edit']);
    Route::get('cms/materials/add', ['uses' => 'Materials@add']);
    Route::post('cms/materials/store', ['uses' => 'Materials@store']);
    Route::get('cms/materials/list', ['uses' => 'Materials@index']);
    Route::get('cms/materials/list/{pageNumber}', ['uses' => 'Materials@index']);

//    delivery_times
    Route::get('cms/deliverytimes/edit/{id}', ['uses' => 'DeliveryTimes@edit']);
    Route::get('cms/deliverytimes/add', ['uses' => 'DeliveryTimes@add']);
    Route::post('cms/deliverytimes/store', ['uses' => 'DeliveryTimes@store']);
    Route::get('cms/deliverytimes/list', ['uses' => 'DeliveryTimes@index']);
    Route::get('cms/deliverytimes/list/{pageNumber}', ['uses' => 'DeliveryTimes@index']);

//    shipping costs
    Route::get('cms/shippingcosts/edit/{id}', ['uses' => 'ShippingCosts@edit']);
    Route::get('cms/shippingcosts/add', ['uses' => 'ShippingCosts@add']);
    Route::post('cms/shippingcosts/store', ['uses' => 'ShippingCosts@store']);
    Route::get('cms/shippingcosts/list', ['uses' => 'ShippingCosts@index']);
    Route::get('cms/shippingcosts/list/{pageNumber}', ['uses' => 'ShippingCosts@index']);

//    images
    Route::get('/cms/unlink_image/{idProductImage}/{idProduct}', ['uses' => 'Products@unlinkImage']);
    Route::get('/cms/image_manager/{folder}/{subfolder}', ['uses' => 'Images@imageManager']);
    Route::get('/cms/image_manager/{folder}', ['uses' => 'Images@imageManager']);
    Route::get('/cms/image_manager', ['uses' => 'Images@imageManager']);

    Route::get('/api/get_images', ['uses' => 'Images@getImagesMeta']);

    //    Route::get('/cms/unlink_image/{idPproductImage}/{idProduct}', ['uses' => 'ShopProductsController@unlinkImage']);
//    Route::get('/cms/image_manager/{folder}/{subfolder}', ['uses' => 'CmsController@imageManager']);
//    Route::get('/cms/image_manager/{folder}', ['uses' => 'CmsController@imageManager']);
//    Route::get('/cms/image_manager', ['uses' => 'CmsController@imageManager']);


//    Route::get('cms/shop_templates/edit/{name_constant}', ['uses' => 'ShopTemplatesController@edit']);
//
//    Route::get('cms/pages/edit/{id}', ['uses' => 'PagesController@edit']);
//    Route::post('cms/pages/store', ['uses' => 'PagesController@store']);
//
//    Route::post('cms/shop_products/store', ['uses' => 'ShopProductsController@store']);
//    Route::get('cms/shop_products/edit/{id}', ['uses' => 'ShopProductsController@edit']);
//    Route::get('cms/shop_products/list', ['uses' => 'ShopProductsController@list']);
//
//    Route::get('cms/shop_orders/list', ['uses' => 'ShopOrdersController@list']);
//    Route::get('cms/shop_customers/list', ['uses' => 'ShopCustomersController@list']);
//
////    Route::post('cms/shop_additional_products/store', ['uses' => 'ShopAdditionalProductsController@store']);
//    Route::get('cms/shop_additional_products/edit/{id}', ['uses' => 'ShopAdditionalProductsController@edit']);
//    Route::get('cms/shop_additional_products/list', ['uses' => 'ShopAdditionalProductsController@list']);
//
//    Route::get('cms/{table}/add', ['uses' => 'CmsController@add']);
//    Route::post('cms/{table}/store', ['uses' => 'CmsController@store']);


});

//Route::get('test', ['uses' => 'ListFilter@index']);

Route::get('categories', function(){
    echo 'Hello from the framework package!';
});

Route::get('listfilter', function(){
    echo 'Hello from the listfilter package!';
});
//Auth::routes();
