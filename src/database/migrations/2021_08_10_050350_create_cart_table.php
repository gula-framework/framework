<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCartTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cart', function (Blueprint $table) {
            $table->id();
            $table->char('id_session')->nullable(false);
            $table->integer('id_product')->nullable(false);
            $table->integer('amount')->nullable(false);
            $table->boolean('deleted')->default(false);
            $table->timestamps();
            $table->index(['id_session', 'id_product', 'deleted'], 'basic_index');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cart');
    }
}
