<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->integer('id_category')->nullable(true);
            $table->integer('id_brand')->nullable(true);
            $table->boolean('stock')->default(false);
            $table->char('name', 50)->nullable(true);
            $table->char('slug', 100)->nullable(true);
            $table->text('description')->nullable(true);
            $table->char('meta_title', 50)->nullable(true);
            $table->char('meta_description', 250)->nullable(true);
            $table->boolean('active')->default(true);
            $table->boolean('deleted')->default(false);
            $table->timestamps();
            $table->index(['id','slug', 'name', 'active', 'deleted', 'id_category'], 'basic_index');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
