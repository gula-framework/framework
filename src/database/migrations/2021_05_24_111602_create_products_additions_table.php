<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsAdditionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products_additions', function (Blueprint $table) {
            $table->id();
            $table->integer('id_product');
            $table->integer('id_addition');
            $table->timestamps();
            $table->index(['id', 'id_addition', 'id_product'], 'basic_index');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products_additions');
    }
}
