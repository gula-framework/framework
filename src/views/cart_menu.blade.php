<li>
    <a class="cart-toggler mini-cart-icon" href="#">
        <i class="fa fa-shopping-cart"></i>
        <span>2</span>
    </a>
    <div class="cart-list">
        <div class="product">
            @foreach($cart as $row)
                <div class="cart-single-product">
                    <a class="product-image" href="#">
                        <img src="img/cart-img/1.jpg" alt="">
                    </a>
                    <div class="product-details">
                        <a href="#" class="remove">
                            <i class="fa fa-times-circle"></i>
                        </a>
                        <div class="product-name">
                        <span class="quantity-formated">
                            <span class="quantity">{{$row->amount}}</span>
                        </span>
                            <a href="#" class="cart-block-product-name" title="Faded Short Sleeves T-shirt">Omschrijving</a>
                        </div>
                        <div class="prices">
                            <span class="price">€  prijs</span>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="cart-prices">
            <div class="cart-prices-line first-line"></div>
            <div class="cart-prices-line last-line">
                <span>Total</span>
                <span class="price pull-right">£ 48.04</span>
            </div>
        </div>
        <p class="cart-buttons">
            <a class="btn btn-default button-small" href="#" title="Check out">
                                                        <span>
                                                            Check out
                                                            <i class="fa fa-angle-right"></i>
                                                        </span>
            </a>
        </p>
    </div>
</li>
