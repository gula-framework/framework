@extends('website-cms::base')

@section('content')
    <div class="meta">
        {{csrf_field()}}
        <input type="hidden" name="model" value="{{$scope['table']}}">

    </div>
    <div class="containerList">
        <div class="row">
            <div class="col-md-12">
                <br/>
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-2">
                                <img src="{{ $scope['icon'] }}"/> {{ $scope['title'] }}
                            </div>
                            <div class="col-md-10">
                                Wijzigen
                            </div>
                        </div>
                    </div>

                    <div class="card-body">
                        <form method="post" action="/cms/{{$scope['table']}}/store" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <input type="hidden" name="id" value="{{$record->id}}">

                            <div class="row">
                                <div class="col-6">
                                    <br/>
                                    <div class="form-group col-8">
                                        <label for="name">Naam van software constante:</label>
                                        <input type="text" class="form-control" name="name_constant"
                                               value="{{$record->name_constant}}" disabled>
                                    </div>
                                    <div class="form-group col-8">
                                        <label for="name">Doel:</label>
                                        <input type="text" class="form-control" name="description"
                                               value="{{$record->description}}" disabled>
                                    </div>
                                    <div class="form-group col-10">
                                        <label for="template">Sjabloon (niet wijzigen)</label>
                                        <textarea name="template"
                                                  class="form-control">{{$record->template}}</textarea>
                                    </div>
                                    <div class="form-group col-2">
                                        <label for="name">Verwijderd:</label>
                                        <select name="deleted" class="form-control">
                                            <option value=0 @if($record->deleted == false) selected @endif>Nee
                                            </option>
                                            <option value=1 @if($record->deleted == true) selected @endif>Ja
                                            </option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <hr/>
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group col-3">
                                <button type="submit" class="btn btn-success">Opslaan</button>
                            </div>
                        </div>
                        <div class="col-6"></div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script src="{{asset('js/select-images.js')}}" rel="stylesheet"></script>
@endsection
