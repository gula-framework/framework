@extends('framework::base')

@section('content')
    <div class="meta">
        {{csrf_field()}}
    </div>
    <div class="containerList">
        <div class="row">
            <div class="col-md-12">
                <br/>
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-2">
                                <img src="{{ $meta['icon'] }}"/> {{ $meta['title'] }}
                            </div>
                            <div class="col-md-10">
                                Wijzigen / Toevoegen
                            </div>
                        </div>
                    </div>

                    <div class="card-body">
                        <form method="post" action="/cms/products/store" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <input type="hidden" name="id" value="{{$record->id}}">

                            <ul class="nav nav-tabs" id="myTab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="general-tab" data-toggle="tab" href="#general"
                                       role="tab" aria-controls="general" aria-selected="true">Algemeen</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="seo-tab" data-toggle="tab" href="#properties" role="tab"
                                       aria-controls="seo" aria-selected="false">Eigenschappen</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="features-tab" data-toggle="tab" href="#features" role="tab"
                                       aria-controls="seo" aria-selected="false">Kenmerken</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="seo-tab" data-toggle="tab" href="#seo" role="tab"
                                       aria-controls="seo" aria-selected="false">SEO</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="images-tab" data-toggle="tab" href="#images" role="tab"
                                       aria-controls="images" aria-selected="false">Afbeeldingen</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="prices-tab" data-toggle="tab" href="#prices" role="tab"
                                       aria-controls="prices" aria-selected="false">Prijzen</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="prices-tab" data-toggle="tab" href="#delivery" role="tab"
                                       aria-controls="deliveries" aria-selected="false">Bezorging</a>
                                </li>
                            </ul>
                            <div class="tab-content" id="myTabContent">

                                @include('framework::product_partials.tab_general')
                                @include('framework::product_partials.tab_properties')
                                @include('framework::product_partials.tab_features')
                                @include('framework::product_partials.tab_seo')
                                @include('framework::product_partials.tab_images')
                                @include('framework::product_partials.tab_prices')
                                @include('framework::product_partials.tab_delivery')

                            </div>
                            <hr/>
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group col-3">
                                        <button type="submit" class="btn btn-success">Opslaan</button>
                                    </div>
                                </div>
                                <div class="col-6"></div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="selectImages" tabindex="-1" role="dialog" aria-labelledby="selectImages"
         aria-hidden="true" style="width: 2000px;">
        <input type="hidden" id="current_folder" value="products">
        <input type="hidden" id="imageId" value="<?php echo rand ( 1000 , 9999 ); ?>"/>

        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalLabel">Kies een afbeelding</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12" id="images_navigate">
                        </div>
                    </div>
                    <div class="row" id="images_miniatures"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Sluiten</button>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('js')
    <script>
        var path_cdn = '{{env('PATH_CDN')}}';
        var app_cdn = '{{env('APP_CDN')}}';

    </script>
    <script src="{{asset('cms/js/select-images.js')}}"></script>
@endsection
