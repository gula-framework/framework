<div class="tab-pane fade" id="prices" role="tabpanel" aria-labelledby="prices-tab">
    <br/>
    <div class="row">
        <div class="col-6">
            <br/>
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>Omschrijving</th>
                    <th style="width: 30%">Prijs</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <input type="hidden" name="prices[0][name]" value="default">
                    <td><input type="text" class="form-control" value="Standaardprijs" readonly="readonly"></td>
                    <td><input type="number" name="prices[0][price]" step="0.05" value="{{$prices[0]['price']}}" min="0"
                               class="form-control">
                    </td>
                </tr>
                @foreach($prices as $key => $price)
                    @if($price['name'] !== 'default')
                        <tr>
                            <td><input type="text" class="form-control" name="prices[{{$key}}][name]"
                                       value="{{$price['name']}}"></td>
                            <td><input type="number" name="prices[{{$key}}][price]" step="0.05" value="{{$price['price']}}"
                                       min="0"
                                       class="form-control">
                            </td>
                        </tr>
                    @endif
                @endforeach
                {{--                @foreach($meta['prices'] as $key => $price)--}}
                {{--                    <tr>--}}
                {{--                        <td>{{$key}}</td>--}}
                {{--                        <td><input type="number" name="price_{{$key}}" step="0.05"--}}
                {{--                                   value="{{$price}}" min="0" class="form-control">--}}
                {{--                        </td>--}}
                {{--                    </tr>--}}
                {{--                @endforeach--}}
                </tbody>
            </table>
        </div>
    </div>
</div>
