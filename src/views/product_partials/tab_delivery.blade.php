<div class="tab-pane fade show" id="delivery" role="tabpanel"
     aria-labelledby="general-tab">
    <div class="row">
        <div class="col-6">
            <br/>
            <div class="form-group col-5">
                <label for="name">Verzendkosten:</label>
                <select name="id_delivery_time" class="form-control">
                    <option value="">Geen</option>
                    @foreach($shippingCosts as $shippingCost)
                    <option value="{{$shippingCost->id}}"
                            @if($record->id_delivery_time === $shippingCost->id) selected @endif>{{$shippingCost->name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group col-5">
                <label for="name">Levertijd:</label>
                <select name="id_shipping_cost" class="form-control">
                    <option value="">Geen</option>
                    @foreach($deliveryTimes as $deliveryTime)
                    <option value="{{$deliveryTime->id}}"
                            @if($record->id_shipping_cost === $deliveryTime->id) selected @endif>{{$deliveryTime->name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group col-3">
                <label for="name">Altijd verzendkosten:</label>
                <select name="forced_shipping_costs" class="form-control">
                    <option value=0 @if($record->forced_shipping_costs == false) selected @endif>Nee
                    </option>
                    <option value=1 @if($record->forced_shipping_costs == true) selected @endif>Ja
                    </option>
                </select>
            </div>
        </div>
    </div>
</div>
