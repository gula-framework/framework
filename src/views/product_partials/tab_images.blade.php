<div class="tab-pane fade" id="images" role="tabpanel" aria-labelledby="images-tab">
    <br/>
    <div class="row">
        <div class="col-4">
            <button id="selectedImage" class="btn btn-info" type="button">Afbeelding toevoegen
            </button>
        </div>
    </div>
    <br/>
    <div class="row" id="productImages">
        @if(count($images) > 0)
            @php
                $c=0;
            @endphp
            @foreach($images as $productImage)
                @php
                    $c++;
                @endphp
                <input type=hidden name="image_{{$c}}"
                       value="{{$productImage->url}}"
                       data-table-id="{{$productImage->id}}"/>
                <div class="col-md-2">

                    <img id="product_image"
                         src="https://cms.gula.nl/resizer/300x300/{{env('APP_CDN')}}/products/{{$productImage->url}}">
                    <div class="disconnectImage">
                        <a href="/cms/unlink_image/{{$productImage->id}}/{{$record->id}}"
                           onclick="return confirmUnlink();"><img
                                src="https://cms.gula.nl/resizer/36x36/cms/icons/disconnect.png"
                                title="Afbeelding loskoppelen"/></a>
                    </div>
                </div>
            @endforeach
        @else
            Er zijn nog geen afbeeldingen gekoppeld!
        @endif
    </div>
</div>
