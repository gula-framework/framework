<div class="tab-pane fade show active" id="general" role="tabpanel"
     aria-labelledby="general-tab">
    <div class="row">
        <div class="col-6">
            <br/>
            <div class="form-group col-8">
                <label for="name">Titel:</label>
                <input type="text" class="form-control" name="name"
                       value="{{$record->name}}">
            </div>
            <div class="form-group col-3">
                <label for="name">Categorie:</label>
                <select name="id_category" class="form-control">
                    <option value="">Geen</option>
                    @foreach($categories as $category)
                        <option value="{{$category->id}}"
                                @if($record->id_category === $category->id) selected @endif>{{$category->name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group col-10">
                <label for="description">Beschrijving</label>
                <textarea name="description"
                          class="form-control">{{$record->description}}</textarea>
            </div>
            <div class="form-group col-10">
                <label for="description_short">Beschrijving kort (tbv
                    overzichtpagina)</label>
                <textarea name="description_short"
                          class="form-control">{{$record->description_short}}</textarea>
            </div>
            <div class="form-group col-2">
                <label for="name">Verwijderd:</label>
                <select name="deleted" class="form-control">
                    <option value=0 @if($record->deleted == false) selected @endif>Nee
                    </option>
                    <option value=1 @if($record->deleted == true) selected @endif>Ja
                    </option>
                </select>
            </div>
        </div>
    </div>
</div>
