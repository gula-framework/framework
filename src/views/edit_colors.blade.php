@extends('framework::base')

@section('content')
    <div class="meta">
        {{csrf_field()}}
        <input type="hidden" name="model" value="colors">

    </div>
    <div class="containerList">
        <div class="row">
            <div class="col-md-12">
                <br/>
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-2">
                                <img src="{{ $meta['icon'] }}"/> {{ $meta['title'] }}
                            </div>
                            <div class="col-md-10">
                                Wijzigen / Toevoegen
                            </div>
                        </div>
                    </div>

                    <div class="card-body">
                        <form method="post" action="/cms/colors/store" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <input type="hidden" name="id" value="{{$record->id}}">

                            <div class="tab-content" id="myTabContent">
                                <div class="row">
                                    <div class="col-6">
                                        <br/>
                                        <div class="form-group col-8">
                                            <label for="name">Naam:</label>
                                            <input type="text" class="form-control" name="name"
                                                   value="{{$record->name}}">
                                        </div>
                                        <div class="form-group col-2">
                                            <label for="name">Verwijderd:</label>
                                            <select name="deleted" class="form-control">
                                                <option value=0 @if($record->deleted == false) selected @endif>Nee
                                                </option>
                                                <option value=1 @if($record->deleted == true) selected @endif>Ja
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr/>
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group col-3">
                                        <button type="submit" class="btn btn-success">Opslaan</button>
                                    </div>
                                </div>
                                <div class="col-6"></div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
